package gofastogt

import (
	"sync/atomic"
	"time"
)

type DurationMsec int64
type UtcTimeMsec int64

func MakeUTCTimestamp() UtcTimeMsec {
	return Time2UtcTimeMsec(time.Now())
}

func Time2UtcTimeMsec(time time.Time) UtcTimeMsec {
	return UtcTimeMsec(time.UnixNano() / 1e6)
}

func UtcTime2Time(msec UtcTimeMsec) time.Time {
	return time.Unix(int64(msec)/1e3, (int64(msec)%1e3)*1e6)
}

type Point struct {
	X int `bson:"x" json:"x"`
	Y int `bson:"y" json:"y"`
}

type Rational struct {
	Num int `bson:"num" json:"num"`
	Den int `bson:"den" json:"den"`
}

type Size struct {
	Width  int `bson:"width" json:"width"`
	Height int `bson:"height" json:"height"`
}

type UniqueIDU64 struct {
	counter uint64
}

func (c *UniqueIDU64) Get() uint64 {
	for {
		val := atomic.LoadUint64(&c.counter)
		if atomic.CompareAndSwapUint64(&c.counter, val, val+1) {
			return val
		}
	}
}

// Max returns the larger of x or y.
func MaxInt(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func MinInt(x, y int) int {
	if x > y {
		return y
	}
	return x
}

// Max returns the larger of x or y.
func MaxInt64(x, y int64) int64 {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func MinInt64(x, y int64) int64 {
	if x > y {
		return y
	}
	return x
}
