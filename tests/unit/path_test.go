package unittests

import (
	"os/user"
	"path/filepath"
	"testing"

	"gitlab.com/fastogt/gofastogt"
)

func TestStableFilePath(t *testing.T) {
	type args struct {
		path string
	}
	usr, err := user.Current()
	if err != nil {
		return
	}
	case1, case3 := filepath.Join(usr.HomeDir, "1.txt"), "/home/sasha/1.txt"
	tests := []struct {
		name    string
		args    args
		want    *string
		wantErr bool
	}{
		{
			name:    "case1",
			args:    args{path: "~/1.txt"},
			want:    &case1,
			wantErr: false,
		},
		{
			name:    "case2",
			args:    args{path: ""},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "case3",
			args:    args{path: "/home/sasha/1.txt"},
			want:    &case3,
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.StableFilePath(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("StableFilePath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != nil {
				if *got != *tt.want {
					t.Errorf("StableFilePath() = %v, want %v", got, tt.want)
				}
			} else {
				if got != tt.want {
					t.Errorf("StableFilePath() = nil, want %v", tt.want)
				}
			}
		})
	}
}

func TestStableDirPath(t *testing.T) {
	type args struct {
		path string
	}
	usr, err := user.Current()
	if err != nil {
		return
	}
	path1, path2, path3, path4, path5 := "~/streamer/", "/streamer", "/home/streamer/", "", "123"
	res1, res2, res3 := filepath.Join(usr.HomeDir, path1[1:])+"/", path2+"/", path3
	tests := []struct {
		name    string
		args    args
		want    *string
		wantErr bool
	}{
		{
			name:    "case_1",
			args:    args{path: path1},
			want:    &res1,
			wantErr: false,
		},
		{
			name:    "case_2",
			args:    args{path: path2},
			want:    &res2,
			wantErr: false,
		},
		{
			name:    "case_3",
			args:    args{path: path3},
			want:    &res3,
			wantErr: false,
		},
		{
			name:    "case_4",
			args:    args{path: path4},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "case_5",
			args:    args{path: path5},
			want:    nil,
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := gofastogt.StableDirPath(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("StableDirPath() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != nil {
				if *got != *tt.want {
					t.Errorf("StableDirPath() = %v, want %v", *got, *tt.want)
				}
			} else {
				if got != tt.want {
					t.Errorf("StableDirPath() = nil, want %v", tt.want)
				}
			}
		})
	}
}
