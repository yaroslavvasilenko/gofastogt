package gofastogt

import (
	"sync"
	"time"
)

type custom_loop_exec_function_t func()

type job struct {
	cancelled bool
	fn        custom_loop_exec_function_t
}

type Timer struct {
	job
	timer *time.Timer
}

type Interval struct {
	job
	ticker   *time.Ticker
	stopChan chan struct{}
}

type IEventLoopObserver interface {
	PreLooped(loop *EventLoop)
	PostLooped(loop *EventLoop)
}

type EventLoop struct {
	jobChan chan custom_loop_exec_function_t
	canRun  bool

	auxJobs     []custom_loop_exec_function_t
	auxJobsLock sync.Mutex
	wakeup      chan struct{}

	stopCond *sync.Cond
	running  bool

	handler *IEventLoopObserver
}

func NewEventLoop(handler IEventLoopObserver) *EventLoop {
	loop := &EventLoop{
		jobChan:  make(chan custom_loop_exec_function_t),
		wakeup:   make(chan struct{}, 1),
		stopCond: sync.NewCond(&sync.Mutex{}),
		handler:  &handler,
	}
	return loop
}

func (loop *EventLoop) Run() {
	loop.stopCond.L.Lock()
	loop.running = true
	loop.stopCond.L.Unlock()

	if loop.handler != nil {
		(*loop.handler).PreLooped(loop)
	}

	loop.canRun = true
	loop.runAux()

	for loop.canRun {
		select {
		case job := <-loop.jobChan:
			job()
			if loop.canRun {
				select {
				case <-loop.wakeup:
					loop.runAux()
				default:
				}
			}
		case <-loop.wakeup:
			loop.runAux()
		}
	}

	if loop.handler != nil {
		(*loop.handler).PostLooped(loop)
	}

	loop.stopCond.L.Lock()
	loop.running = false
	loop.stopCond.L.Unlock()
	loop.stopCond.Broadcast()
}

func (loop *EventLoop) runAux() {
	loop.auxJobsLock.Lock()
	jobs := loop.auxJobs
	loop.auxJobs = nil
	loop.auxJobsLock.Unlock()
	for _, job := range jobs {
		job()
	}
}

func (loop *EventLoop) Stop() {
	loop.jobChan <- func() {
		loop.canRun = false
	}

	loop.stopCond.L.Lock()
	for loop.running {
		loop.stopCond.Wait()
	}
	loop.stopCond.L.Unlock()
}

func (loop *EventLoop) ExecInLoopThread(fn custom_loop_exec_function_t) {
	loop.addAuxJob(fn)
}

func (loop *EventLoop) addAuxJob(fn custom_loop_exec_function_t) {
	loop.auxJobsLock.Lock()
	loop.auxJobs = append(loop.auxJobs, fn)
	loop.auxJobsLock.Unlock()
	select {
	case loop.wakeup <- struct{}{}:
	default:
	}
}

func (loop *EventLoop) AddTimeout(f custom_loop_exec_function_t, timeout time.Duration) *Timer {
	t := &Timer{
		job: job{fn: f},
	}
	t.timer = time.AfterFunc(timeout, func() {
		loop.jobChan <- func() {
			loop.doTimeout(t)
		}
	})

	return t
}

func (loop *EventLoop) AddInterval(f custom_loop_exec_function_t, timeout time.Duration) *Interval {
	i := &Interval{
		job:      job{fn: f},
		ticker:   time.NewTicker(timeout),
		stopChan: make(chan struct{}),
	}

	go i.run(loop)
	return i
}

func (loop *EventLoop) ClearTimeout(t *Timer) {
	if t != nil && !t.cancelled {
		t.timer.Stop()
		t.cancelled = true
	}
}

func (loop *EventLoop) ClearInterval(i *Interval) {
	if i != nil && !i.cancelled {
		i.cancelled = true
		close(i.stopChan)
	}
}

func (loop *EventLoop) doTimeout(t *Timer) {
	if !t.cancelled {
		t.fn()
		t.cancelled = true
	}
}

func (loop *EventLoop) doInterval(i *Interval) {
	if !i.cancelled {
		i.fn()
	}
}

func (i *Interval) run(loop *EventLoop) {
L:
	for {
		select {
		case <-i.stopChan:
			i.ticker.Stop()
			break L
		case <-i.ticker.C:
			loop.jobChan <- func() {
				loop.doInterval(i)
			}
		}
	}
}
